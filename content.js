let timeHelp = 0, // текущее время
    timeRemain = 0; // остаток времени

    document.addEventListener('DOMContentLoaded', function() {
      // Получение элементов после полной загрузки страницы
      const filter = document.getElementById("filter");
      const atWork = document.getElementById("At work");
      const toDo = document.getElementById("To do");
      const all = document.getElementById("All");
      const updateButton = document.getElementById('update-button');
      const linksTasks = document.getElementById("links-tasks");
    
      // Добавление обработчика для кнопки
      updateButton.addEventListener('click', Update);
    
      //функция вывода списка задач
      function PrintListTask(res) {
        let taskListHtml = "";
        let number = 1;
        for (let i = 0; i < res.links.length; i++) {
          let link = res.links[i].self;
          let id = res.links[i].id;
          if (atWork.checked && (id === '3' || id === '4')) {
            taskListHtml += (taskListHtml === "") ? "<div class='div-center' style='margin-bottom: 10px;'>Список Ваших задач</div>" : "";
            taskListHtml += number + ". <a class='link' href='" + link + "' target='_blank'>" + link + "</a><br/>";
            number = number + 1;
          }
          else if (toDo.checked && (id === '1' || id === '2')) {
            taskListHtml += (taskListHtml === "") ? "<div class='div-center' style='margin-bottom: 10px;'>Список Ваших задач</div>" : "";
            taskListHtml += number + ". <a class='link' href='" + link + "' target='_blank'>" + link + "</a><br/>";
            number = number + 1;
          }
          else if (all.checked && id) {
            taskListHtml += (taskListHtml === "") ? "<div class='div-center' style='margin-bottom: 10px;'>Список Ваших задач</div>" : "";
            taskListHtml += number + ". <a class='link' href='" + link + "' target='_blank'>" + link + "</a><br/>";
            number = number + 1;
          }
        }
        linksTasks.innerHTML = taskListHtml;
      }
    
      //функция обновления времени и списка в окне
      function Update() {
        console.log("сработало нажатие");
        // Отправка сообщения background
        function sendUpdateMessage() {
          chrome.runtime.sendMessage({ message: 1 }, (res) => {
            console.log("res: ", res);
            if (!res.authStatus || res.response == -1) {
              // Если res равен null, повторяем вызов функции через 1 секунду
              setTimeout(sendUpdateMessage, 300);
              return;
            }

            // Обработка ответа
            timeHelp = res.response / 60;
            timeRemain = 8 * 60 - timeHelp;
            console.log("timeHelp ", timeHelp);
            console.log("timeRem ", timeRemain);

            // Вывод времени в расширении браузера
            if (res.authStatus !== undefined && res.authStatus) // если авторизованы
            {
              if (cont !== null) {
                if (timeRemain <= 0) // выводим время
                  cont.innerHTML = "Вы отчитали требуемое время работы. Сейчас оно составляет: " + timeHelp + " минут.";
                else
                  cont.innerHTML = "В Вашем журнале работ указано " + timeHelp + " минут. Необходимо отчитать еще - " + timeRemain + " минут!";

                if (res.links.length > 0) {
                  PrintListTask(res);
                  const radioButtons = document.getElementsByName("contact");
                  for (let i = 0; i < radioButtons.length; i++) {
                    radioButtons[i].addEventListener("change", function() {
                      // Выводим ссылки на задачи
                      PrintListTask(res);
                    });
                  }
                }
                else {
                  linksTasks.style.display = "none";
                  filter.style.display = "none";
                  updateButton.style.display = "none";
                }
              }
            }
            else {
              // просим авторизоваться для вывода информации
              cont.innerHTML = "Необходимо авторизоваться на сайте Jira для того, чтобы пользоваться данным расширением!";
              linksTasks.style.display = "none";
              filter.style.display = "none";
              updateButton.style.display = "none";
            }
          });
        }

        sendUpdateMessage();
      }
    
      Update();
    });