const urlGarpix = "https://pm.garpix.com/rest/api/2/"; 
let Links = [{ self: "", id: 0, name: "" }]; //массив для ссылок на объекты

let userName = " ", // имя пользователя
    timeSpent = -1, // залогированное время
    stat; //статус авторизации

// Класс для работы с fetch
class API {
  // GET-запрос с передачей cookie
  static get(url, cookies) {
    return fetch(url, {
      method: "GET",
      headers: {
        Cookie: "JSESSIONID=" + cookies[0].value, // первый cookie с именем "JSESSIONID"
      },
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          console.log("Error HTTP: " + response.status);
        }
      })
      .then(function (data) {
        return data;
      })
      .catch(function (error) {
        console.error("Error:", error);
      });
  }
}

//функция для проверки авторизации пользователя в Jira 
const checkedAuthorization = () => { 
  return fetch(urlGarpix + "myself")
  .then(response => {
    if (response.status === 401) {
      console.log("Пользователь не авторизован");
      timeSpent = 0;
      stat = false;
    } 
    else {
      console.log("Пользователь авторизован");
      stat = true;
    }
  })
}

// Функция проверки времени
const checkedDate = (date) => {
  let dateToday = new Date();
  if (date.getDate() === dateToday.getDate() && date.getMonth() === dateToday.getMonth() && date.getFullYear() === dateToday.getFullYear())
    return true;
  return false;
};

// Основная функция
const mainFunction = async () => {
  // Получение cookie
  chrome.cookies.getAll({ domain: "pm.garpix.com", name: "JSESSIONID" }, async (cookies) => {
      console.log("cookie: ", cookies);

      // Получение имени пользователя
      userName = await API.get(urlGarpix + "myself", cookies);
      console.log("userName: ", userName);

      // Получение списка задач пользователя
      let project = await API.get(urlGarpix + "search?jql=assignee=" + '"' + userName.name + '"', cookies);
      console.log("project: ", project);

      timeSpent = 0;

      for (let i = 0; i < project.issues.length; i++) {
        // Получение списка логов всех пользователей
        let issue = await API.get(urlGarpix + "issue/" + project.issues[i].id + "/worklog", cookies);
        console.log("issue: ", issue);

        Links[i] = { self: "https://pm.garpix.com/browse/" + project.issues[i].key, id: project.issues[i].fields.status.id, name: project.issues[i].fields.status.name };

        for (let j = 0; j < issue.worklogs.length; j++) {
          // Подсчет времени конкретного пользователя в момент времени "сегодня"
          if (issue.worklogs[j].author.name === userName.name && checkedDate(new Date(issue.worklogs[j].created))) {
            console.log("worklog: ", issue.worklogs[j]);
            timeSpent = timeSpent + issue.worklogs[j].timeSpentSeconds;
          }
        }
        console.log("links: ", Links);
        console.log("time: ", timeSpent); 
      }
    }
  );
};

// Получение сообщения от content
chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  // Обработка сообщения
  mainFunction();
  checkedAuthorization();
  sendResponse({ response: timeSpent, links: Links, authStatus: stat});
});

// // Функция таймера
// setInterval(function () {
//   mainFunction();
//   console.log("MessageTime");
// }, 30000);